import base64

hcm_host = "localhost"
hcm_username = "root"
hcm_password = base64.b64decode("")
hcm_database = "dssm"

portal_host = "localhost"
portal_username = "root"
portal_password = base64.b64decode("")
portal_database = "dssm"

def hcm_query(hostname):
    #return "select hostname,serial,vendor,ldev,ltype,device,mname,lname,lgroup from disks where hostname like '"+hostname+"' and ldev not like '-' group by ldev order by ldev;"
    return "select hostname,replace(serial,'%20',' ') as serial,replace(vendor,'%2C',',') as vendor,ldev,ltype,device,mname,lname,lgroup from disks where hostname like '"+hostname+"' and ldev not like '-' group by ldev order by ldev;"

def portal_query(hostname):
    #return "select hostname,serial,vendor,ldev,ltype,device,mname,lname,lgroup from disks where hostname like '"+hostname+"' and ldev not like '-' group by ldev order by ldev;"
    return "select hostname,replace(serial,'%20',' ') as serial,replace(vendor,'%2C',',') as vendor,ldev,ltype,device,mname,lname,lgroup from disks where hostname like '"+hostname+"' and ldev not like '-' group by ldev order by ldev;"

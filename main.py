import sys, re, MySQLdb, config
from flask import Flask, request, render_template, jsonify

hcm_host = config.hcm_host
hcm_user = config.hcm_username
hcm_passwd = config.hcm_password
hcm_db = config.hcm_database

portal_host = config.portal_host
portal_user = config.portal_username
portal_passwd = config.portal_password
portal_db = config.portal_database

regex = re.compile(r'[^a-zA-Z0-9]')

app = Flask(__name__)

@app.route('/')
def my_form():
    return render_template('form.html')

@app.route('/', methods=['POST'])
def my_form_post():
    form_hostname = request.form['hostname']
    hostname = form_hostname.strip()
    form_database = request.form['database']
    if (regex.search(hostname) == None):
        if ( form_database == "hcm" ):
            dbs = MySQLdb.connect(hcm_host,hcm_user,hcm_passwd,hcm_db)
            sql_query = config.hcm_query(hostname)
            cur = dbs.cursor()
            cur.execute(sql_query)
            count = cur.execute(sql_query)
            if count > 0:
                data = cur.fetchall()
            else:
                return hostname+" - Unable to find Hostname in HCM Database"
        else:
            dbs = MySQLdb.connect(portal_host,portal_user,portal_passwd,portal_db)
            sql_query = config.portal_query(hostname)
            cur = dbs.cursor()
            cur.execute(sql_query)
            count = cur.execute(sql_query)
            if count > 0:
                data = cur.fetchall()
            else:
                return hostname+" - Unable to find Hostname in OSE Portal Database"
    else:
        return hostname+" - Invalid HostName provided as input"
    return database(data)

@app.route("/")
def database(data):
    return render_template('template.html', data=data)

if __name__ == "__main__":
    app.run('0.0.0.0',5678,debug=True)
